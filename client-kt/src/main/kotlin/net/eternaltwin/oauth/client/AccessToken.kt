package net.eternaltwin.oauth.client

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString

@Serializable
data class AccessToken(
  @SerialName("access_token")
  val accessToken: String,
  @SerialName("refresh_token")
  val refreshToken: String? = null,
  @SerialName("expires_in")
  val expiresIn: Int,
  @SerialName("token_type")
  val tokenType: TokenType,
) {
  companion object {
    @JvmStatic
    fun fromJsonString(jsonString: String): AccessToken = JSON_FORMAT.decodeFromString(jsonString)

    @JvmStatic
    fun toJsonString(value: AccessToken): String = JSON_FORMAT.encodeToString(value)
  }
}
