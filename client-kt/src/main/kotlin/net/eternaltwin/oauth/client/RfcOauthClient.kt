package net.eternaltwin.oauth.client

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import java.net.URI
import java.nio.charset.Charset
import java.util.*

class RfcOauthClient(authorizationEndpoint: URI, tokenEndpoint: URI, callbackEndpoint: URI, clientId: String, clientSecret: String) {
  private val authorizationEndpoint: HttpUrl
  private val tokenEndpoint: HttpUrl
  private val callbackEndpoint: HttpUrl
  private val clientId: String
  private val clientSecret: String
  private val client: OkHttpClient

  init {
    this.authorizationEndpoint = authorizationEndpoint.toString().toHttpUrl()
    this.tokenEndpoint = tokenEndpoint.toString().toHttpUrl()
    this.callbackEndpoint = callbackEndpoint.toString().toHttpUrl()
    this.clientId = clientId
    this.clientSecret = clientSecret
    this.client = OkHttpClient()
  }

  /**
   * @param string $scope Scope string.
   * @param string $state Client state.
   * @return UriInterface Authorization URI where the user should be redirected.
   */
  fun getAuthorizationUri(scope: String, state: String): URI {
    return this.authorizationEndpoint
      .newBuilder()
      .addQueryParameter("access_type", "offline")
      .addQueryParameter("response_type", "code")
      .addQueryParameter("redirect_uri", this.callbackEndpoint.toString())
      .addQueryParameter("client_id", this.clientId)
      .addQueryParameter("scope", scope)
      .addQueryParameter("state", state)
      .build()
      .toUri()
  }

  fun getAccessTokenSync(code: String): AccessToken {
    val body = OauthAccessTokenRequest(
      clientId = this.clientId,
      clientSecret = this.clientSecret,
      redirectUri = this.callbackEndpoint.toString(),
      code = code,
      grantType = "authorization_code"
    )

    val bodyJson = OauthAccessTokenRequest.toJsonString(body);

    val request = Request.Builder()
      .url(this.tokenEndpoint)
      .post(bodyJson.toRequestBody(MEDIA_TYPE_JSON))
      .header("Authorization", this.getAuthorizationHeader())
      .build()

    this.client.newCall(request).execute().use { response ->
      if (!response.isSuccessful) {
        throw RuntimeException("Unexpected code $response")
      }
      return AccessToken.fromJsonString(response.body!!.string())
    }
  }

  private fun getAuthorizationHeader(): String {
    val loginBytes = this.clientId.toByteArray(Charset.forName("UTF-8"))
    val passwordBytes = this.clientSecret.toByteArray(Charset.forName("UTF-8"))
    val credentialsBytes = loginBytes + COLON_UTF8_BYTES + passwordBytes
    val encoded = Base64.getEncoder().encodeToString(credentialsBytes)
    return "Basic $encoded"
  }

  companion object {
    private val COLON_UTF8_BYTES = ":".toByteArray(Charset.forName("UTF-8"))
    private val MEDIA_TYPE_JSON = "application/json; charset=utf-8".toMediaType()
  }
}
