package net.eternaltwin.oauth.client

import kotlinx.serialization.Serializable

@Serializable
enum class TokenType {
  Bearer;
}
